package com.firebase.androidchat;

/*
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
*/
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import java.util.HashMap;
import java.util.Map;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;


public class AuthorizationActivity extends AppCompatActivity implements View.OnClickListener,  GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
{
    private static final String TAG = AuthorizationActivity.class.getSimpleName();

    /* TextView that is used to display information about the logged in user */
    private TextView mLoggedInStatusTextView;

    /* A dialog that is presented until the Firebase authentication finished. */
    private ProgressDialog mAuthProgressDialog;

    /* A reference to the Firebase */
    private Firebase mFirebaseRef;

    /* Data from the authenticated user */
    private AuthData mAuthData;

    /* Listener for Firebase session changes */
    private Firebase.AuthStateListener mAuthStateListener;

    /* The login button for Facebook */
    private LoginButton mFacebookLoginButton;
    /* The callback manager for Facebook */
    private CallbackManager mFacebookCallbackManager;
    /* Used to track user logging in/out off Facebook */
    private AccessTokenTracker mFacebookAccessTokenTracker;


    /* Request code used to invoke sign in user interactions for Google+ */
   // public static final int RC_GOOGLE_LOGIN = 1;
    /* Client used to interact with Google APIs. */
   // private GoogleApiClient mGoogleApiClient;
    /* A flag indicating that a PendingIntent is in progress and prevents us from starting further intents. */
   // private boolean mGoogleIntentInProgress;
    /* Track whether the sign-in button has been clicked so that we know to resolve all issues preventing sign-in
     * without waiting. */
    //private boolean mGoogleLoginClicked;
    /* Store the connection result from onConnectionFailed callbacks so that we can resolve them when the user clicks
     * sign-in. */
    //private ConnectionResult mGoogleConnectionResult;
    //private SignInButton mGoogleLoginButton;
    //public static final int RC_TWITTER_LOGIN = 2;
    //private Button mTwitterLoginButton;
    private Button mPasswordLoginButton;
    private Button mAnonymousLoginButton;

    public boolean firsttime=true;


    TextView tVLogin,updateTextView;
    EditText eTLogin, eTPassword;
    Button btnLogin, btnRegistration,btnVKLogin;

    ProgressBar progressBar;
    String loginEnt;

    SharedPreferences prefs = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization_main);

        //элементы управления
        updateTextView = (TextView) findViewById(R.id.updateTextView);
        tVLogin = (TextView) findViewById(R.id.tVLogin);
        eTLogin = (EditText) findViewById(R.id.eTLogin);
        eTPassword = (EditText) findViewById(R.id.eTPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRegistration = (Button) findViewById(R.id.btnRegistration);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        btnLogin.setOnClickListener(this);
        btnRegistration.setOnClickListener(this);


        //proverim na pervii zapusk, chtobi pokazivat help dialogs
        prefs = getSharedPreferences("com.evgmobile.purechat", MODE_PRIVATE);
        firsttime=prefs.getBoolean("firstrun", true);


        socialAuthorization();  //авторизация через соц сети

        Firebase.setAndroidContext(this);


        //если интернет доступен и это первый запуск
        if (isInternetAvailable() && firsttime) {


            progressBar.setVisibility(ProgressBar.VISIBLE);
            btnLogin.setEnabled(false);
            //btnVKLogin.setEnabled(false);
            mFacebookLoginButton.setEnabled(false);
            mPasswordLoginButton.setEnabled(false);
            mAnonymousLoginButton.setEnabled(false);

            updateTextView.setText(getString(R.string.baseupdate));
            //загрузим базу


            //пометим, что первый запуск произошел
            prefs.edit().putBoolean("firstrun", false).commit();
        }

        else if (!isInternetAvailable() && firsttime) {
            Toast.makeText(getApplicationContext(),
                    R.string.nobase,
                    Toast.LENGTH_LONG).show();
            finish();
        }



    }


    void onDownloadFinished() {

        //if(salonsDownloaded && medDownloaded && mastersDownloaded) {
            progressBar.setVisibility(ProgressBar.INVISIBLE);
            btnLogin.setEnabled(true);
            //btnVKLogin.setEnabled(true);
            mFacebookLoginButton.setEnabled(true);
            mPasswordLoginButton.setEnabled(true);
            mAnonymousLoginButton.setEnabled(true);
            updateTextView.setText("");
        //}
    }


    private void socialAuthorization()
    {
         /* Load the Facebook login button and set up the tracker to monitor access token changes */
        mFacebookCallbackManager = CallbackManager.Factory.create();
        mFacebookLoginButton = (LoginButton) findViewById(R.id.login_with_facebook);
        mFacebookAccessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                Log.i(TAG, "Facebook.AccessTokenTracker.OnCurrentAccessTokenChanged");
                AuthorizationActivity.this.onFacebookAccessTokenChange(currentAccessToken);
            }
        };


        mPasswordLoginButton = (Button) findViewById(R.id.login_with_password);
        mPasswordLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginWithPassword();
            }
        });

        /* Load and setup the anonymous login button */
        //работает только эта кнопка
        mAnonymousLoginButton = (Button) findViewById(R.id.login_anonymously);
        mAnonymousLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginAnonymously();
            }
        });

        mLoggedInStatusTextView = (TextView) findViewById(R.id.login_status);

        /* Create the Firebase ref that is used for all authentication with Firebase */
        mFirebaseRef = new Firebase(getResources().getString(R.string.firebase_url));

        /* Setup the progress dialog that is displayed later when authenticating with Firebase */

        /*oshibka na show
        mAuthProgressDialog = new ProgressDialog(this);
        mAuthProgressDialog.setTitle("Loading");
        mAuthProgressDialog.setMessage("Authenticating with Firebase...");
        mAuthProgressDialog.setCancelable(false);
        mAuthProgressDialog.show();
        */
        mAuthStateListener = new Firebase.AuthStateListener() {
            @Override
            public void onAuthStateChanged(AuthData authData) {
                //mAuthProgressDialog.hide();
                setAuthenticatedUser(authData);
            }
        };

        mFirebaseRef.addAuthStateListener(mAuthStateListener);



    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Map<String, String> options = new HashMap<String, String>();


            /* Otherwise, it's probably the request by the Facebook login button, keep track of the session */
            mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);

    }

    void finishLogin(boolean success) {
        if (success) {
            //v sessii zapisat master ili user?


            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            startActivity(intent);

        } else {
            tVLogin.setText(R.string.nouser);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnLogin:

                loginEnt = eTLogin.getText().toString().toLowerCase();
                String passwordEnt = eTPassword.getText().toString();

                mFirebaseRef.authWithPassword(loginEnt, passwordEnt, new AuthResultHandler("password"));

                //blokiruem interface na vremya proverki parolya
                progressBar.setVisibility(ProgressBar.VISIBLE);
                btnLogin.setEnabled(false);
                //btnVKLogin.setEnabled(false);
                mFacebookLoginButton.setEnabled(false);
                mPasswordLoginButton.setEnabled(false);
                mAnonymousLoginButton.setEnabled(false);
                break;
            case R.id.btnRegistration:
                //Intent intent = new Intent(this, RegistrationActivity.class);
                //startActivity(intent);
                break;
        }
    }
    public boolean isInternetAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            Toast.makeText(getApplicationContext(),
                    R.string.internetnotwork,
                    Toast.LENGTH_LONG).show();
            return false;
        } else
            return true;
    }





    @Override
    protected void onDestroy() {
        super.onDestroy();
        // if user logged in with Facebook, stop tracking their token
        if (mFacebookAccessTokenTracker != null) {
            mFacebookAccessTokenTracker.stopTracking();
        }

        // if changing configurations, stop tracking firebase session.
        mFirebaseRef.removeAuthStateListener(mAuthStateListener);
    }

    /**
     * Unauthenticate from Firebase and from providers where necessary.
     */
    private void logout() {
        if (this.mAuthData != null) {
            /* logout of Firebase */
            mFirebaseRef.unauth();
            /* Logout of any of the Frameworks. This step is optional, but ensures the user is not logged into
             * Facebook/Google+ after logging out of Firebase. */
            if (this.mAuthData.getProvider().equals("facebook")) {
                /* Logout from Facebook */
                LoginManager.getInstance().logOut();
            }
            /* Update authenticated user and show login buttons */
            setAuthenticatedUser(null);
            //pokajem knopki
            //!!vosstanovit mFacebookLoginButton.setVisibility(View.VISIBLE);
            //!!vosstanovit mPasswordLoginButton.setVisibility(View.VISIBLE);
            mAnonymousLoginButton.setVisibility(View.VISIBLE);
            mLoggedInStatusTextView.setVisibility(View.INVISIBLE);
        }
    }


    /**
     * This method will attempt to authenticate a user to firebase given an oauth_token (and other
     * necessary parameters depending on the provider)
     */
    /*
    private void authWithFirebase(final String provider, Map<String, String> options) {
        if (options.containsKey("error")) {
            showErrorDialog(options.get("error"));
        } else {
            mAuthProgressDialog.show();
            if (provider.equals("twitter")) {
                // if the provider is twitter, we pust pass in additional options, so use the options endpoint
                mFirebaseRef.authWithOAuthToken(provider, options, new AuthResultHandler(provider));
            } else {
                // if the provider is not twitter, we just need to pass in the oauth_token
                mFirebaseRef.authWithOAuthToken(provider, options.get("oauth_token"), new AuthResultHandler(provider));
            }
        }
    }
*/
    /**
     * Once a user is logged in, take the mAuthData provided from Firebase and "use" it.
     */
    private void setAuthenticatedUser(AuthData authData) {
        if (authData != null) {
            /* Hide all the login buttons */
            mFacebookLoginButton.setVisibility(View.GONE);
            mPasswordLoginButton.setVisibility(View.GONE);
            mAnonymousLoginButton.setVisibility(View.GONE);
            mLoggedInStatusTextView.setVisibility(View.VISIBLE);
            /* show a provider specific status text */
            String name = null;
            if (authData.getProvider().equals("facebook")
                    || authData.getProvider().equals("google")
                    || authData.getProvider().equals("twitter")) {
                name = (String) authData.getProviderData().get("displayName");
                //тут редиректим на активность


                // TODO: Temporary code. To remove and rewrite
                //!!!tut opredelit gorod
                /*
                appState.setCurCityCoord(55.739241, 37.624143);
                appState.setCurCity("Moscow");*/

                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(intent);
                //тут авторизация анонимная?
            } else if (authData.getProvider().equals("anonymous")
                    || authData.getProvider().equals("password")) {
                name = authData.getUid();


                // TODO: Temporary code. To remove and rewrite
                //!!!tut opredelit gorod
                /*
                appState.setCurCityCoord(55.739241, 37.624143);
                appState.setCurCity("Moscow");*/

                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(intent);

            } else {
                Log.e(TAG, "Invalid provider: " + authData.getProvider());
            }
            if (name != null) {
                mLoggedInStatusTextView.setText("Logged in as " + name + " (" + authData.getProvider() + ")");
                //тут редиректим на активность

            }
        } else {
            /* No authenticated user show all the login buttons */
            //!!vosstanovit mFacebookLoginButton.setVisibility(View.VISIBLE);
            //mGoogleLoginButton.setVisibility(View.VISIBLE);
            //mTwitterLoginButton.setVisibility(View.VISIBLE);
            //!!vosstanovit mPasswordLoginButton.setVisibility(View.VISIBLE);
            mAnonymousLoginButton.setVisibility(View.VISIBLE);
            mLoggedInStatusTextView.setVisibility(View.GONE);
        }
        this.mAuthData = authData;
        /* invalidate options menu to hide/show the logout button */
        supportInvalidateOptionsMenu();
    }

    /**
     * Show errors to users
     */
    private void showErrorDialog(String message) {
        new AlertDialog.Builder(this)
                .setTitle("Error")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    /**
     * Utility class for authentication results
     */
    private class AuthResultHandler implements Firebase.AuthResultHandler {

        private final String provider;

        public AuthResultHandler(String provider) {
            this.provider = provider;
        }

        @Override
        public void onAuthenticated(AuthData authData) {
            //mAuthProgressDialog.hide();
            Log.i(TAG, provider + " auth successful");
            setAuthenticatedUser(authData);

            //получим айди юзера
            String userid=authData.getUid();
            //Firebase keyRef = new Firebase(Config.FIREBASE_USER_URL + "/" + userid);

            //тут читаем в класс соответсвия
            //если вошли по паролю
            /*
            if (provider.equals("password")) {
                keyRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        //SpotUser user = dataSnapshot.getValue(SpotUser.class);

                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {
                        // No-op
                    }
                });
            }
            //вход без анонимно
            else {

            }*/

            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            startActivity(intent);
        }

        @Override
        public void onAuthenticationError(FirebaseError firebaseError) {
//            mAuthProgressDialog.hide();
            showErrorDialog(firebaseError.toString());
        }
    }

    /* ************************************
     *             FACEBOOK               *
     **************************************
     */
    private void onFacebookAccessTokenChange(AccessToken token) {
        if (token != null) {
            //mAuthProgressDialog.show();
            mFirebaseRef.authWithOAuthToken("facebook", token.getToken(), new AuthResultHandler("facebook"));
        } else {
            // Logged out of Facebook and currently authenticated with Firebase using Facebook, so do a logout
            if (this.mAuthData != null && this.mAuthData.getProvider().equals("facebook")) {
                mFirebaseRef.unauth();
                setAuthenticatedUser(null);
            }
        }
    }


    @Override
    public void onConnected(final Bundle bundle) {
        // Connected with Google API, use this to authenticate with Firebase
        //getGoogleOAuthTokenAndLogin();
    }


    @Override
    public void onConnectionFailed(ConnectionResult result) {

    }

    @Override
    public void onConnectionSuspended(int i) {
        // ignore
    }


    /* ************************************
     *              USERID              *
     **************************************
     */
    public void loginWithPassword() {
       // mAuthProgressDialog.show();
        mFirebaseRef.authWithPassword("test@firebaseuser.com", "test1234", new AuthResultHandler("password"));
    }

    /* ************************************
     *             ANONYMOUSLY            *
     **************************************
     */
    private void loginAnonymously() {
        //mAuthProgressDialog.show();
        mFirebaseRef.authAnonymously(new AuthResultHandler("anonymous"));

    }
    /*
    @override
    public void OnResume(){
        logout();
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        logout();
    }*/
}


