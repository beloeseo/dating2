package com.firebase.androidchat;

import android.app.Application;
import android.content.Context;

//import com.facebook.FacebookSdk;
import com.facebook.FacebookSdk;
import com.firebase.client.Firebase;
import java.util.ArrayList;


public class GlobalVariable extends Application
{


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
       // MultiDex.install(this);
    }


    private static GlobalVariable mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        //VKSdk.initialize(this);
        Firebase.setAndroidContext(this);
        FacebookSdk.sdkInitialize(this);
    }




    // ---------

    public static synchronized GlobalVariable getInstance() {
        return mInstance;
    }
}//End Class
