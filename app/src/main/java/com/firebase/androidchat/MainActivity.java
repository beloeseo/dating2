package com.firebase.androidchat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firebase.androidchat.robospice.SampleSpiceService;
import com.firebase.androidchat.spot.SpotUser;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;
import com.octo.android.robospice.request.listener.RequestProgress;
import com.octo.android.robospice.request.listener.RequestProgressListener;
import com.octo.android.robospice.request.simple.BitmapRequest;
import com.octo.android.robospice.request.simple.SimpleTextRequest;

import com.octo.android.robospice.request.springandroid.SpringAndroidSpiceRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;



public class MainActivity extends AppCompatActivity {

    ArrayList<SpotUser> manyuserspots = new ArrayList<SpotUser>();
    public static MyAppAdapter myAppAdapter;
    public static ViewHolder viewHolder;
    private ArrayList<Data> array;
    private SwipeFlingAdapterView flingContainer;
    int curindex=0;

    private SimpleTextRequest flatRequest;
    private BitmapRequest imageRequest;
    private SpiceManager spiceManager = new SpiceManager(SampleSpiceService.class);
    private static final String EARTH_IMAGE_CACHE_KEY = "image";




    public class MyRequest extends SpringAndroidSpiceRequest<SpotUser> {

        public MyRequest(){
            super(SpotUser.class);
        }

        @Override
        public SpotUser loadDataFromNetwork() throws Exception {
            MultiValueMap<String, String> parameters = new LinkedMultiValueMap<String, String>();
            parameters.set("foo", "bar");

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(parameters, headers);

            return getRestTemplate().postForObject("http://docbooking.ru/dating/userjson.txt", request, SpotUser.class);
        }
    }




    public final class LoremRequestListener implements RequestListener<String> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {

        }

        @Override
        public void onRequestSuccess(String result) {


            String[] users=result.split("\r\n");

            //manyuserspots = new ArrayList<SpotUser>();

            //array = new ArrayList<>();


            for (int i=0;i<users.length;i++)
            {
                String[]user=users[i].split(";");
                String h1=user[0];
               manyuserspots.add(new SpotUser(user[0],user[1],user[2]));

                array.add(new Data(user[1], user[2]));

            }

            //GlobalVariable appState = ((GlobalVariable) getActivity().getApplicationContext());
            //appState.setSpots(manyflatspots);


        }
    }

    public final class ImageRequestListener implements RequestListener<Bitmap>, RequestProgressListener {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            //Toast.makeText(AuthorizationActivity.this, "failure", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onRequestSuccess(final Bitmap bitmap) {

        }

        @Override
        public void onRequestProgressUpdate(RequestProgress progress) {
            switch (progress.getStatus()) {
                case LOADING_FROM_NETWORK:
                    setProgressBarIndeterminate(false);
                    setProgress((int) (progress.getProgress() * 10000));
                    break;
                default:
                    break;
            }

        }
    }

    @Override
    public void onStart() {
        spiceManager.start(this);
        super.onStart();

        getSpiceManager().execute(flatRequest, "txt", DurationInMillis.ONE_MINUTE, new LoremRequestListener());
//        getSpiceManager().execute(imageRequest, EARTH_IMAGE_CACHE_KEY, 5 * DurationInMillis.ONE_MINUTE, new ImageRequestListener());
    }

    @Override
    public void onStop() {
        spiceManager.shouldStop();
        super.onStop();

    }

    protected SpiceManager getSpiceManager() {
        return spiceManager;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);





        flatRequest = new SimpleTextRequest(Config.USERS_URL);
        //File cacheFile = new File(getCacheDir(), "earth.jpg");
        //imageRequest = new BitmapRequest("http://earthobservatory.nasa.gov/blogs/elegantfigures/files/2011/10/globe_west_2048.jpg", cacheFile);

        manyuserspots = new ArrayList<SpotUser>();


        array = new ArrayList<>();
        array.add(new Data("http://docbooking.ru/dating/img/kseniadavydova.jpg", "Приятная девушка"));
        array.add(new Data("http://docbooking.ru/dating/img/kudryashova.jpg", "Люблю мороженое"));
        array.add(new Data("http://docbooking.ru/dating/img/kuznecova.jpg", "Потанцуем?"));
        array.add(new Data("http://docbooking.ru/dating/img/i.jpg", "Познакомлюсь с девушкой"));

        manyuserspots.add(new SpotUser("7475","http://docbooking.ru/dating/img/kseniadavydova.jpg", "Приятная девушка"));
        manyuserspots.add(new SpotUser("7476","http://docbooking.ru/dating/img/kudryashova.jpg", "Люблю мороженое"));
        manyuserspots.add(new SpotUser("7477","http://docbooking.ru/dating/img/kuznecova.jpg", "Потанцуем?"));
        manyuserspots.add(new SpotUser("7478","http://docbooking.ru/dating/img/i.jpg", "Познакомлюсь с девушкой"));

        //getRestTemplate().postForObject("http://www.yoursite.com", request, MyResponse.class);


        flingContainer = (SwipeFlingAdapterView) findViewById(R.id.frame);

        myAppAdapter = new MyAppAdapter(array, MainActivity.this);
        flingContainer.setAdapter(myAppAdapter);
        flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
            @Override
            public void removeFirstObjectInAdapter() {

            }

            @Override
            public void onLeftCardExit(Object dataObject) {
                array.remove(0);
                //String her=(String) dataObject;
                myAppAdapter.notifyDataSetChanged();
                //Do something on the left!
                //You also have access to the original object.
                //If you want to use it just cast it (String) dataObject
                curindex++;
                if (curindex<4){
                    SpotUser her=manyuserspots.get(curindex);}

            }

            @Override
            public void onRightCardExit(Object dataObject) {

                array.remove(0);
                myAppAdapter.notifyDataSetChanged();
                curindex++;
                if (curindex<4){
                SpotUser her=manyuserspots.get(curindex);}
                Intent intent = new Intent(getBaseContext(), PayActivity.class);
                startActivity(intent);
            }

            @Override
            public void onAdapterAboutToEmpty(int itemsInAdapter) {

            }

            @Override
            public void onScroll(float scrollProgressPercent) {

                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.background).setAlpha(0);
                view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
            }
        });


        // Optionally add an OnItemClickListener
        flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
            @Override
            public void onItemClicked(int itemPosition, Object dataObject) {

                View view = flingContainer.getSelectedView();
                view.findViewById(R.id.background).setAlpha(0);

                myAppAdapter.notifyDataSetChanged();
            }
        });
    }

    public static class ViewHolder {
        public static FrameLayout background;
        public TextView DataText;
        public ImageView cardImage;


    }

    public class MyAppAdapter extends BaseAdapter {


        public List<Data> parkingList;
        public Context context;

        private MyAppAdapter(List<Data> apps, Context context) {
            this.parkingList = apps;
            this.context = context;
        }

        @Override
        public int getCount() {
            return parkingList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View rowView = convertView;


            if (rowView == null) {

                LayoutInflater inflater = getLayoutInflater();
                rowView = inflater.inflate(R.layout.item, parent, false);
                // configure view holder
                viewHolder = new ViewHolder();
                viewHolder.DataText = (TextView) rowView.findViewById(R.id.bookText);
                viewHolder.background = (FrameLayout) rowView.findViewById(R.id.background);
                viewHolder.cardImage = (ImageView) rowView.findViewById(R.id.cardImage);
                rowView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.DataText.setText(parkingList.get(position).getDescription() + "");

            Glide.with(MainActivity.this).load(parkingList.get(position).getImagePath()).into(viewHolder.cardImage);

            return rowView;
        }
    }
}
