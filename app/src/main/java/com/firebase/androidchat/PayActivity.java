package com.firebase.androidchat;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.firebase.androidchat.R;
import com.firebase.androidchat.util.IabHelper;
import com.firebase.androidchat.util.IabResult;
import com.firebase.androidchat.util.Purchase;
import com.firebase.androidchat.util.Inventory;


public class PayActivity extends AppCompatActivity {

    private static final String TAG =
            "InAppBilling";
    IabHelper mHelper;
    static final String ITEM_SKU = "com.evgmobile.pure.now";


    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result,
                                          Purchase purchase)
        {
            if (result.isFailure()) {
                // Handle error
                return;
            }
            else if (purchase.getSku().equals(ITEM_SKU)) {
                consumeItem();
                //buyButton.setEnabled(false);
            }

        }
    };
    public void consumeItem() {
        mHelper.queryInventoryAsync(mReceivedInventoryListener);
        Intent intent = new Intent(getBaseContext(), ChatActivity.class);
        startActivity(intent);
        // /!!!покажем список девушек или мальчиков
        /*
        showcontacts=true;
        GlobalVariable appState = ((GlobalVariable) getActivity().getApplicationContext());
        appState.setShowcontacts(true);
        */
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {

            if (result.isFailure()) {
                // Handle failure
            } else {
                mHelper.consumeAsync(inventory.getPurchase(ITEM_SKU),
                        mConsumeFinishedListener);
            }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener =
            new IabHelper.OnConsumeFinishedListener() {
                public void onConsumeFinished(Purchase purchase,
                                              IabResult result) {

                    if (result.isSuccess()) {
                        //clickButton.setEnabled(true);
                        mHelper.queryInventoryAsync(mReceivedInventoryListener);
                        Intent intent = new Intent(getBaseContext(), MainActivity.class);
                        startActivity(intent);
                    } else {
                        // handle error
                    }
                }
            };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //super.onActivityResult(requestCode, resultCode, data);


        if (!mHelper.handleActivityResult(requestCode,
                resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button buttonBuy = (Button) findViewById(R.id.btnBuyNow);
        buttonBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mHelper.launchPurchaseFlow(PayActivity.this, ITEM_SKU, 10001,
                        mPurchaseFinishedListener, "mypurchasetoken");
            }

        });


        String base64EncodedPublicKey =
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmTqHn3W7kIMapiKQVVRRi9DCt0jlRN7jREpKzoqX4AK2Z1IorGcOEsg/AnzEFvSbf+50Mm9o62JjkZdM75eihqOiFfLAalqEdk8Lv+EgLvm66irA8gXDmKV35mdUg9jfKpFXEz+u8MIC/musGVItgEEQ19LdyOshArnpyiplwcQtxIeXXTzJA1K+MDszM2APthWFKPu+ghLVgPr5Jvh7Tw4EYSwhFnGX7e6jZuCvP8rKJxCP30EoIbhALZ/dNpy+8c2nJFRo8kuPgi5KKpJMVSHAOXLvVF9uuLkxro7b09AeGE5CeGZG4wczJj0wzsPUZ9O84ADaQY6Jo/3gtk3brwIDAQAB";
        mHelper = new IabHelper(this, base64EncodedPublicKey);

        mHelper.startSetup(new
                                   IabHelper.OnIabSetupFinishedListener() {
                                       public void onIabSetupFinished(IabResult result) {
                                           if (!result.isSuccess()) {
                                               Log.d(TAG, "In-app Billing setup failed: " +
                                                       result);
                                           } else {
                                               Log.d(TAG, "In-app Billing is set up OK");
                                           }
                                       }
                                   });

    }

}
