package com.firebase.androidchat.spot;


import android.os.Parcel;
import android.os.Parcelable;


public class SpotUser implements Parcelable {
    private String id;
    private String description;
    private String image;


    public SpotUser(String id, String desciption, String image) {
        this.id =id;
        this.description = desciption;
        this.image = image;

    }

    public SpotUser(Parcel in) {
        String[] data = new String[1];
        in.readStringArray(data);
        id = data[0];

    }


    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }
    public String getId(){return id; }



    //parcelable
    @Override
    public int describeContents() {
        return 0; //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
//To change body of implemented methods use File | Settings | File Templates.
        out.writeStringArray(new String[]{description, image, id});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        @Override
        public SpotUser createFromParcel(Parcel in) {
            return new SpotUser(in);
        }

        @Override
        public SpotUser[] newArray(int size) {
            return new SpotUser[size];
        }
    };

}
